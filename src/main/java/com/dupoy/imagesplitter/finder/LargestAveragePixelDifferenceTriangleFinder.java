package com.dupoy.imagesplitter.finder;

import com.dupoy.imagesplitter.graphics.Triangle;

import java.util.List;

public class LargestAveragePixelDifferenceTriangleFinder
    implements ITriangleFinder
{
    public Triangle findWorstTriangle(List<Triangle> triangles)
    {
        Triangle worstTriangle = null;
        double worstDifference = 0.0;
        for (Triangle triangle : triangles)
        {
            // can't split an already split one
            if (triangle.isSplit())
            {
                continue;
            }

            // is this the new worst triangle
            int numberOfPixels = triangle.getNumberOfPixels();
            if (numberOfPixels < 100)
            {
                continue;
            }
            double difference = triangle.getTotalDifference() / numberOfPixels;
            if (worstTriangle == null || difference > worstDifference)
            {
                System.out.println("new worst triangle: difference: " + difference + ", pixels: " + numberOfPixels);
                worstTriangle = triangle;
                worstDifference = difference;
            }
        }
        return worstTriangle;
    }
}
