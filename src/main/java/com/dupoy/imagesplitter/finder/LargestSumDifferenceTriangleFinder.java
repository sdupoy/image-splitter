package com.dupoy.imagesplitter.finder;

import com.dupoy.imagesplitter.config.Config;
import com.dupoy.imagesplitter.graphics.Triangle;
import com.google.inject.Inject;

import java.awt.*;
import java.util.List;

public class LargestSumDifferenceTriangleFinder
    implements ITriangleFinder
{
    private final Config config;

    @Inject
    public LargestSumDifferenceTriangleFinder(Config config)
    {
        this.config = config;
    }

    public Triangle findWorstTriangle(List<Triangle> triangles)
    {
        Triangle worstTriangle = null;
        for (Triangle triangle : triangles)
        {
            // can't split an already split one
            if (triangle.isSplit())
            {
                continue;
            }

            // is the triangle too small to split?
            if (!triangle.isSplittable())
            {
                continue;
            }

            // is this the new worst triangle
            if (worstTriangle == null || triangle.getTotalDifference() > worstTriangle.getTotalDifference())
            {
                worstTriangle = triangle;
            }
        }
        return worstTriangle;
    }
}
