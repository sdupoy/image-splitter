package com.dupoy.imagesplitter.finder;

import com.dupoy.imagesplitter.graphics.Triangle;

import java.util.List;

public interface ITriangleFinder
{
    Triangle findWorstTriangle(List<Triangle> triangles);
}
