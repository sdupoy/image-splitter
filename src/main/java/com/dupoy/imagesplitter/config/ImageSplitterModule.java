package com.dupoy.imagesplitter.config;

import com.dupoy.imagesplitter.finder.ITriangleFinder;
import com.dupoy.imagesplitter.finder.LargestAveragePixelDifferenceTriangleFinder;
import com.dupoy.imagesplitter.finder.LargestSumDifferenceTriangleFinder;
import com.google.inject.AbstractModule;

public class ImageSplitterModule
    extends AbstractModule
{
    @Override
    public void configure()
    {
        // note: concrete implementations will be resolved without explicit bindings

        // need to pick a finder
        bind(ITriangleFinder.class).to(LargestSumDifferenceTriangleFinder.class);
        //bind(ITriangleFinder.class).to(LargestAveragePixelDifferenceTriangleFinder.class);
    }
}