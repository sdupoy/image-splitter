package com.dupoy.imagesplitter.config;

public class Config
{
    private final String rootDirectoryPath;
    private final String sourceFileName;
    //private final int maximumWorkingImageDimension;
    private final int numberOfTriangles;
    private final int progressInterval;
    private final int longestSideLengthOfSmallestTriangleToSplit;

    public Config()
    {
        this.rootDirectoryPath = "/Users/scott/Pictures/_Triangles";
        this.sourceFileName = "volcania.jpg";
        //this.maximumWorkingImageDimension = 512;
        this.numberOfTriangles = 5000;
        this.progressInterval = 200;
        this.longestSideLengthOfSmallestTriangleToSplit = 10;
    }

    public String getRootDirectoryPath()
    {
        return this.rootDirectoryPath;
    }

    public String getSourceFileName()
    {
        return this.sourceFileName;
    }

//    public int getMaximumWorkingImageDimension()
//    {
//        return this.maximumWorkingImageDimension;
//    }

    public int getNumberOfTriangles()
    {
        return this.numberOfTriangles;
    }

    public int getProgressInterval()
    {
        return progressInterval;
    }

    public int getLongestSideLengthOfSmallestTriangleToSplit()
    {
        return this.longestSideLengthOfSmallestTriangleToSplit;
    }
}
