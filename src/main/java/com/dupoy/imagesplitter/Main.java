package com.dupoy.imagesplitter;

import com.dupoy.imagesplitter.config.ImageSplitterModule;
import com.dupoy.imagesplitter.core.Controller;
import com.google.inject.Guice;

import java.io.IOException;

public class Main
{
    public static void main(String[] args)
        throws IOException
    {
        Guice.createInjector(new ImageSplitterModule()).getInstance(Controller.class).run();
    }
}
