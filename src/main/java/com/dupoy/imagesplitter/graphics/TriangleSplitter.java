package com.dupoy.imagesplitter.graphics;

import com.google.inject.Inject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class TriangleSplitter
{
    private final TriangleBuilder triangleBuilder;

    @Inject
    public TriangleSplitter(TriangleBuilder triangleBuilder)
    {
        this.triangleBuilder = triangleBuilder;
    }

    public List<Triangle> split(Triangle triangle, BufferedImage targetImage)
    {
        // assume it's well formed

        // for now always splitting along longest axis
        Polygon polygon = triangle.getPolygon();
        int x0 = polygon.xpoints[0];
        int y0 = polygon.ypoints[0];
        int x1 = polygon.xpoints[1];
        int y1 = polygon.ypoints[1];
        int x2 = polygon.xpoints[2];
        int y2 = polygon.ypoints[2];

        double len0to1 = this.getLengthQuantifier(x0, y0, x1, y1);
        double len0to2 = this.getLengthQuantifier(x0, y0, x2, y2);
        double len1to2 = this.getLengthQuantifier(x1, y1, x2, y2);

        Polygon polygon0 = new Polygon();
        Polygon polygon1 = new Polygon();

        if (len0to1 > len0to2 && len0to1 > len1to2)
        {
            // len0to1 is longest. one end on each polygon
            polygon0.addPoint(x0, y0);
            polygon1.addPoint(x1, y1);

            // shared point is 2, so both polygons get it
            polygon0.addPoint(x2, y2);
            polygon1.addPoint(x2, y2);

            // each polygon gets the midpoint
            int midX = this.getMidPoint(x0, x1);
            int midY = this.getMidPoint(y0, y1);
            polygon0.addPoint(midX, midY);
            polygon1.addPoint(midX, midY);
        }
        else if (len0to2 > len0to1 && len0to2 > len1to2)
        {
            // len0to2 is longest. one end on each polygon
            polygon0.addPoint(x0, y0);
            polygon1.addPoint(x2, y2);

            // shared point is 1, so both polygons get it
            polygon0.addPoint(x1, y1);
            polygon1.addPoint(x1, y1);

            // each polygon gets the midpoint
            int midX = this.getMidPoint(x0, x2);
            int midY = this.getMidPoint(y0, y2);
            polygon0.addPoint(midX, midY);
            polygon1.addPoint(midX, midY);
        }
        else
        {
            // len1to2 must be longest. one end on each polygon
            polygon0.addPoint(x1, y1);
            polygon1.addPoint(x2, y2);

            // shared point is 0, so both polygons get it
            polygon0.addPoint(x0, y0);
            polygon1.addPoint(x0, y0);

            // each polygon gets the midpoint
            int midX = this.getMidPoint(x1, x2);
            int midY = this.getMidPoint(y1, y2);
            polygon0.addPoint(midX, midY);
            polygon1.addPoint(midX, midY);
        }

        List<Triangle> splitTriangles = new ArrayList<Triangle>();
        splitTriangles.add(this.triangleBuilder.build(polygon0, targetImage));
        splitTriangles.add(this.triangleBuilder.build(polygon1, targetImage));
        return splitTriangles;
    }

    private double getLengthQuantifier(double x0, double y0, double x1, double y1)
    {
        double x = x1 - x0;
        double y = y1 - y0;
        return x * x + y * y;
    }

    private int getMidPoint(int x0, int x1)
    {
        return x0 + (int)(((double)x1 - (double)x0) / 2.0);
    }
}
