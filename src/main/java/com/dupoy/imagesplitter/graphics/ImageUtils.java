package com.dupoy.imagesplitter.graphics;

import com.dupoy.imagesplitter.config.Config;
import com.google.inject.Inject;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageUtils
{
    private final Config config;
    private final ColorAverager colorAverager;

    @Inject
    public ImageUtils(
        Config config,
        ColorAverager colorAverager)
    {
        this.config = config;
        this.colorAverager = colorAverager;
    }

//    public BufferedImage getScaledTargetImage(BufferedImage inputImage)
//    {
//        int max = this.config.getMaximumWorkingImageDimension();
//        int inputWidth = inputImage.getWidth();
//        int inputHeight = inputImage.getHeight();
//        if (inputWidth <= max && inputHeight <= max)
//        {
//            // no need to scale
//            return inputImage;
//        }
//
//        // get dimensions to scale to
//        int scaledWidth;
//        int scaledHeight;
//        if (inputWidth > inputHeight)
//        {
//            scaledWidth = max;
//            scaledHeight = (int)(((double)max / (double)inputWidth) * (double)inputHeight);
//        }
//        else
//        {
//            scaledWidth = (int)(((double)max / (double)inputHeight) * (double)inputWidth);
//            scaledHeight = max;
//        }
//
//        // scale the image
//        System.out.println("scaling image from (" + inputWidth + " x " + inputHeight + ") to (" + scaledWidth + " x " + scaledHeight + ")");
//        BufferedImage scaledImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
//        Graphics2D graphics = scaledImage.createGraphics();
//        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//        graphics.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
//        graphics.dispose();
//        return scaledImage;
//    }

    public BufferedImage createInitialWorkingImage(int width, int height, Color backgroundColor)
    {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = image.createGraphics();
        graphics.setColor(backgroundColor);
        graphics.fillRect(0, 0, width, height);
        graphics.dispose();
        return image;
    }
}
