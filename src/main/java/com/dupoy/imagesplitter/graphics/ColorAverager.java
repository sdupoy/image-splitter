package com.dupoy.imagesplitter.graphics;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class ColorAverager
{
    public Color getAverage(BufferedImage image)
    {
        int[] rgbs = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        int n = rgbs.length;
        long red = 0;
        long green = 0;
        long blue = 0;
        for (int i = 0; i < n; i++)
        {
            Color inputPixelColor = new Color(rgbs[i]);
            red += inputPixelColor.getRed();
            green += inputPixelColor.getGreen();
            blue += inputPixelColor.getRed();
        }
        red /= n;
        green /= n;
        blue /= n;
        return new Color((int) red, (int) green, (int) blue);
    }
}
