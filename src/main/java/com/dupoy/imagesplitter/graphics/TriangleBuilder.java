package com.dupoy.imagesplitter.graphics;

import com.dupoy.imagesplitter.config.Config;
import com.google.inject.Inject;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class TriangleBuilder
{
    private final Config config;

    @Inject
    public TriangleBuilder(Config config)
    {
        this.config = config;
    }

    Triangle build(Polygon polygon, BufferedImage targetImage)
    {
        // traverse the bounding box, get the colors of the pixels that are in the triangle
        List<Color> targetColors = new ArrayList<Color>();
        long red = 0;
        long green = 0;
        long blue = 0;
        Rectangle bounds = polygon.getBounds();
        for (int x = bounds.x; x < bounds.x + bounds.width; x++)
        {
            for (int y = bounds.y; y < bounds.y + bounds.height; y++)
            {
                // pixel isn't in the polygon
                if (!polygon.contains(x, y))
                {
                    continue;
                }

                // add components to totals so we can work out average color
                Color color = new Color(targetImage.getRGB(x, y));
                int r = color.getRed();
                int g = color.getGreen();
                int b = color.getBlue();
                red += r;
                green += g;
                blue += b;

                // cache so we can calculate the color difference once we know the average
                targetColors.add(color);
            }
        }

        int numberOfPixels = targetColors.size();

        Color averageColor = Color.WHITE;
        if (numberOfPixels != 0)
        {
            red /= numberOfPixels;
            green /= numberOfPixels;
            blue /= numberOfPixels;
            averageColor = new Color((int)red, (int)green, (int)blue);
        }

        double difference = this.getDifference(targetColors, averageColor);

        return new Triangle(this.config, polygon, averageColor, numberOfPixels, difference);
    }

    private double getDifference(List<Color> targetColors, Color averageColor)
    {
        long difference = 0;
        for (Color targetColor : targetColors)
        {
            int redDiff = targetColor.getRed() - averageColor.getRed();
            int greenDiff = targetColor.getGreen() - averageColor.getGreen();
            int blueDiff = targetColor.getBlue() - averageColor.getBlue();
            difference += (redDiff * redDiff) + (greenDiff * greenDiff) + (blueDiff * blueDiff); // square diffs
        }
        //return Math.sqrt((double)difference / targetColors.size());
        return (double)difference; // no need to square?
    }
}
