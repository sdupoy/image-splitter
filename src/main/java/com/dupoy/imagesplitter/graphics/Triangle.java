package com.dupoy.imagesplitter.graphics;

import com.dupoy.imagesplitter.config.Config;

import java.awt.*;

public class Triangle
{
    private final Polygon polygon;
    private final Color averageColor;
    private final int numberOfPixels;
    private final double totalDifference;
    private boolean split;
    private boolean isSplittable;

    public Triangle(
        Config config,
        Polygon polygon,
        Color averageColor,
        int numberOfPixels,
        double totalDifference)
    {
        this.polygon = polygon;
        this.averageColor = averageColor;
        this.numberOfPixels = numberOfPixels;
        this.totalDifference = totalDifference;
        this.split = false;
        this.isSplittable = this.isSplittable(config);
    }

    public boolean isSplit()
    {
        return this.split;
    }

    public void setSplit(boolean split)
    {
        this.split = split;
    }

    public boolean isSplittable()
    {
        return this.isSplittable;
    }

    public Polygon getPolygon()
    {
        return this.polygon;
    }

    public double getTotalDifference()
    {
        return this.totalDifference;
    }

    public int getNumberOfPixels()
    {
        return this.numberOfPixels;
    }

    public void drawEdge(Graphics2D graphics)
    {
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        graphics.setColor(this.averageColor);
        Stroke originalStroke = graphics.getStroke();

        graphics.setStroke(new BasicStroke(2.0f));
        graphics.draw(this.polygon);

        graphics.setStroke(originalStroke);
    }

    public void fillShape(Graphics2D graphics)
    {
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.setColor(this.averageColor);
        graphics.fill(this.polygon);
    }

    private boolean isSplittable(Config config)
    {
        int x0 = this.polygon.xpoints[0];
        int y0 = this.polygon.ypoints[0];
        int x1 = this.polygon.xpoints[1];
        int y1 = this.polygon.ypoints[1];
        int x2 = this.polygon.xpoints[2];
        int y2 = this.polygon.ypoints[2];

        double sqLen0to1 = this.getSquaredLength(x0, y0, x1, y1);
        double sqLen0to2 = this.getSquaredLength(x0, y0, x2, y2);
        double sqLen1to2 = this.getSquaredLength(x1, y1, x2, y2);

        double maxSqLen = sqLen0to1 > sqLen0to2 ? sqLen0to1 : sqLen0to2;
        maxSqLen = maxSqLen > sqLen1to2 ? maxSqLen : sqLen1to2;

        double maxLen = Math.sqrt(maxSqLen);
        return maxLen > config.getLongestSideLengthOfSmallestTriangleToSplit();
    }

    private double getSquaredLength(int x0, int y0, int x1, int y1)
    {
        double x = x1 - x0;
        double y = y1 - y0;
        return x * x + y * y;
    }
}
