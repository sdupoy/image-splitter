package com.dupoy.imagesplitter.graphics;

import com.google.inject.Inject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class InitialSplitter
{
    private final TriangleBuilder triangleBuilder;

    @Inject
    public InitialSplitter(TriangleBuilder triangleBuilder)
    {
        this.triangleBuilder = triangleBuilder;
    }

    public List<Triangle> split(BufferedImage targetImage)
    {
        // initial split line: (0,0) x (w,h) (could do or (0,h) x (w,0))
        System.out.println("TODO: initial split in direction which is best");
        int w = targetImage.getWidth();
        int h = targetImage.getHeight();

        Polygon polygon0 = new Polygon();
        polygon0.addPoint(0, 0);
        polygon0.addPoint(0, h);
        polygon0.addPoint(w, h);

        Polygon polygon1 = new Polygon();
        polygon1.addPoint(0, 0);
        polygon1.addPoint(w, 0);
        polygon1.addPoint(w, h);

        List<Triangle> triangles = new ArrayList<Triangle>();
        triangles.add(this.triangleBuilder.build(polygon0, targetImage));
        triangles.add(this.triangleBuilder.build(polygon1, targetImage));
        return triangles;
    }
}
