package com.dupoy.imagesplitter.io;

import com.dupoy.imagesplitter.config.Config;
import com.google.inject.Inject;

import java.io.File;

public class FilePaths
{
    private final Config config;
    private final File rootDirectory;
    private final File progressDirectory;
    private final File sourceFile;

    @Inject
    public FilePaths(Config config)
    {
        this.config = config;
        this.rootDirectory = new File(this.config.getRootDirectoryPath());
        this.progressDirectory = new File(this.rootDirectory, this.getFileNameWithoutSuffix());
        this.sourceFile = new File(this.rootDirectory, this.config.getSourceFileName());
    }

    public File getRootDirectory()
    {
        return this.rootDirectory;
    }

    public File getProgressDirectory()
    {
        return this.progressDirectory;
    }

    public File getSourceFile()
    {
        return this.sourceFile;
    }

    public File getProgressFilePath(int count)
    {
        String progressFileName = this.getFileNameWithoutSuffix() + "-" + count + ".png";
        return new File(this.getProgressDirectory(), progressFileName);
    }

    private String getFileNameWithoutSuffix()
    {
        String sourceFileName = this.config.getSourceFileName();
        int dotIndex = sourceFileName.indexOf('.');
        return sourceFileName.substring(0, dotIndex);
    }
}
