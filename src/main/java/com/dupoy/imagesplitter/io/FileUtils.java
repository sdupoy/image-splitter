package com.dupoy.imagesplitter.io;

import com.dupoy.imagesplitter.config.Config;
import com.dupoy.imagesplitter.graphics.ImageUtils;
import com.dupoy.imagesplitter.graphics.Triangle;
import com.google.inject.Inject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileUtils
{
    private final Config config;
    private final FilePaths filePaths;
    private final ImageUtils imageUtils;

    @Inject
    public FileUtils(
        Config config,
        FilePaths filePaths,
        ImageUtils imageUtils)
    {
        this.config = config;
        this.filePaths = filePaths;
        this.imageUtils = imageUtils;
    }

    public BufferedImage readSourceImage()
        throws IOException
    {
        System.out.println("reading source image: " + this.filePaths.getSourceFile().getAbsolutePath());
        BufferedImage image = ImageIO.read(this.filePaths.getSourceFile());
        System.out.println("image dimensions: " + image.getWidth() + " x " + image.getHeight());
        return image;
    }

    public void writeProgressImage(int width, int height, Color backgroundColor, int progressCount, List<Triangle> triangles)
        throws IOException
    {
        File file = this.filePaths.getProgressFilePath(progressCount);
        System.out.println("writing progress file: " + file.getName());
        BufferedImage image = this.imageUtils.createInitialWorkingImage(width, height, backgroundColor);
        Graphics2D graphics = image.createGraphics();

        // first pass is edges
        for (Triangle triangle : triangles)
        {
            // ignore split triangles
            if (triangle.isSplit())
            {
                continue;
            }
            triangle.drawEdge(graphics);
        }

        // second pass is fills
        for (Triangle triangle : triangles)
        {
            // ignore split triangles
            if (triangle.isSplit())
            {
                continue;
            }
            triangle.fillShape(graphics);
        }

        ImageIO.write(image, "png", file);
    }

    public void ensureDirectories()
    {
        System.out.println("ensuring required directories exist");
        this.ensureDirectory(this.filePaths.getRootDirectory());
        this.ensureDirectory(this.filePaths.getProgressDirectory());
    }

    private void ensureDirectory(File directory)
    {
        if (!directory.exists())
        {
            System.out.println("creating directory: " + directory.getAbsolutePath());
            directory.mkdirs();
        }
    }
}
