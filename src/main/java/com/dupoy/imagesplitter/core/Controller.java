package com.dupoy.imagesplitter.core;

import com.dupoy.imagesplitter.config.Config;
import com.dupoy.imagesplitter.finder.ITriangleFinder;
import com.dupoy.imagesplitter.graphics.*;
import com.dupoy.imagesplitter.io.FileUtils;
import com.google.inject.Inject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

public class Controller
{
    private final Config config;
    private final FileUtils fileUtils;
    private final ImageUtils imageUtils;
    private final ColorAverager colorAverager;
    private final InitialSplitter initialSplitter;
    private final ITriangleFinder triangleFinder;
    private final TriangleSplitter triangleSplitter;

    @Inject
    public Controller(
        Config config,
        FileUtils fileUtils,
        ImageUtils imageUtils,
        ColorAverager colorAverager,
        InitialSplitter initialSplitter,
        ITriangleFinder triangleFinder,
        TriangleSplitter triangleSplitter)
    {
        this.config = config;
        this.fileUtils = fileUtils;
        this.imageUtils = imageUtils;
        this.colorAverager = colorAverager;
        this.initialSplitter = initialSplitter;
        this.triangleFinder = triangleFinder;
        this.triangleSplitter = triangleSplitter;
    }

    public void run()
        throws IOException
    {
        // idea:
        // - allow worst triangle to be split along each length (in the middle) and
        //   then select the optimal split that way. minimise the length that can be split (4 px?)
        //    (also has benefit of making the triangles non-unifom so won't form rows, etc)
        //     - also do say 25%, 50%, 75% way along each line, ensure lines aren't too short

        // - better idea (?) to minimise size of triangles (=> null returned if max line length
        //   is too small)

        System.out.println("running image splitter");
        this.fileUtils.ensureDirectories();

        BufferedImage sourceImage = this.fileUtils.readSourceImage();
        BufferedImage targetImage = sourceImage; // do the work at full size
        //BufferedImage targetImage = this.imageUtils.getScaledTargetImage(sourceImage);
        Color backgroundColor = this.colorAverager.getAverage(targetImage);

        // initial split to get triangles instead of rectangles
        List<Triangle> triangles = this.initialSplitter.split(targetImage);

        // now keep on splitting the triangles
        int count = triangles.size();
        while (count < this.config.getNumberOfTriangles())
        {
            //System.out.println("iteration: triangle count: " + count);
            Triangle worstTriangle = this.triangleFinder.findWorstTriangle(triangles);
            if (worstTriangle == null)
            {
                System.out.println("early exit with " + count + " triangles");
                break;
            }
            for (Triangle splitTriangle : this.triangleSplitter.split(worstTriangle, targetImage))
            {
                triangles.add(splitTriangle);
            }
            worstTriangle.setSplit(true);
            count++;

            if (count % this.config.getProgressInterval() == 0 && count < this.config.getNumberOfTriangles())
            {
                this.fileUtils.writeProgressImage(
                    targetImage.getWidth(),
                    targetImage.getHeight(),
                    backgroundColor,
                    count,
                    triangles);
            }
        }

        // final render
        this.fileUtils.writeProgressImage(
            targetImage.getWidth(),
            targetImage.getHeight(),
            backgroundColor,
            count,
            triangles);

        System.out.println("finished");
    }
}
